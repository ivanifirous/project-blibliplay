package com.blibli.future.bliblipage;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class BliPlayProperties {

    public String getProperties(String variable){
        Properties prop = new Properties();

        try (InputStream input = new FileInputStream("blibliplay.properties")) {
            prop.load(input);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return prop.getProperty(variable);
    }
}
