package com.blibli.future.bliblipage;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class BlibliHomePage {
    WebDriver webDriver;

    By xpathElementLainKali = By.xpath("//button[contains(text(),'Lain kali')]");
    By xpathElementBtnBliPlay = By.xpath("//div[13]//a[1]//div[1]//img[1]");
    By xpathElementBtnMasuk = By.id("gdn-login-registrasi");
    By xpathElementBtnLihatSemua = By.xpath(" //div[@class='favourite__header']//a[contains(text(),'Lihat Semua')]");
    //By xpathElementEmail = By.className("input--email");
   // By xpathElementPass = By.className("input--password");
    By xpathElementEmail = By.xpath("//input[@placeholder='Masukkan email']");
    By xpathElementPass = By.xpath("//input[@placeholder='Masukkan password']");
    By xpathElementBtnSubmitLogin = By.xpath("//button[@class='button-submit']");

    public BlibliHomePage(WebDriver webDriver){
        this.webDriver = webDriver;
    }

    public void openHomePage() throws InterruptedException {
        Thread.sleep(2000);
        webDriver.get("https://www.blibli.com");
    }

    public void closeIklan(){
        WebElement btnLainKali = webDriver.findElement(xpathElementLainKali);
        btnLainKali.click();
    }

    public void openBliPlay(){
        WebElement btnLihatSemua = webDriver.findElement(xpathElementBtnLihatSemua);
        btnLihatSemua.click();
        WebElement btnBliPlay = webDriver.findElement(xpathElementBtnBliPlay);
        btnBliPlay.click();
    }

    public void btnMasukClick() throws InterruptedException {
        Thread.sleep(3000);
        WebElement btnMasuk = webDriver.findElement(xpathElementBtnMasuk);
        btnMasuk.click();
    }

    public void btnEmailClick(String searchkey) throws InterruptedException {
        Thread.sleep(2000);
        WebElement email = webDriver.findElement(xpathElementEmail);
        email.sendKeys(searchkey);
    }

    public void btnPassClick(String searchkey){
        WebElement pass = webDriver.findElement(xpathElementPass);
        pass.sendKeys(searchkey);
    }

    public void btnSubmitLoginClick() throws InterruptedException {
        Thread.sleep(3000);
        WebElement btnMasuk = webDriver.findElement(xpathElementBtnSubmitLogin);
        btnMasuk.click();
    }
}
