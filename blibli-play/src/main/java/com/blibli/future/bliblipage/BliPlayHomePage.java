package com.blibli.future.bliblipage;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.Keys;

import java.util.List;

public class BliPlayHomePage {
    WebDriver webDriver;

    By xpathElementBtnMasuk = By.xpath("//span[@class='navbar_user-logout-blue']");
    By xpathElementEmail = By.xpath("//input[@placeholder='Email']");
    By xpathElementPass = By.xpath("//input[@placeholder='Password']");
    By xpathElementBtnSubmitLogin = By.xpath("//button[@class='content__loginForm-submit']");
    By xpathElementTxtBox = By.xpath("//input[@placeholder='Nonton apa hari ini?']");
    By getXpathElementTxtBoxIcon = By.xpath("//div[@class='navbar_right-icon']//img");
    By xpathElementLogout = By.xpath("//div[@class='navbar_user-loggedin-details-list'][contains(text(),'Keluar')]");
    By xpathElementBtnLogout = By.xpath("//div[@class='logout__options-choice'][contains(text(),'Keluar')]");

    // 1st
    By xpathElementBtnTonton = By.xpath("//a[@class='videoInfo__right-buttons-watchseason']");
    By xpathElementViewAll = By.xpath("//div[@class='search__container-categoryContainer-heading-viewAll']");
    By xpathElementPlay =By.xpath("//img[@id='player-splash-splash-img-splash-play-button-splash-play-button']");

    // 2nd
    By xpathElementBtnvideo = By.xpath("//div[contains(text(),'Blibli Indonesia Open 2018 Finals')]");
    By xpathElementCloseBtnShare = By.xpath("//div[@class='close']");

    // 3rd
    By xpathElementBtnLike = By.xpath("//div[@class='videoInfo__right-badgesAndActions-actions-like-notLikedAction']");
    By xpathElementBtnDislike = By.xpath("//div[@class='videoInfo__right-badgesAndActions-actions-dislike-notDislikedAction']");
    By xpathElementBtnShare = By.xpath("//div[@class='videoInfo__right-badgesAndActions-actions-share']");

    // 4th
    By xpathElementTentangBliPlay = By.xpath("//li[contains(text(),'Tentang Blibliplay')]");
    By xpathElementBelanjadiBlibli = By.xpath("//div[@class='footer-subfooter__section-blibliLogo']//a//img");

    // 7th
    By xpathElementTheBigStart = By.xpath("//div[@class='card__title'][contains(text(),'The Big Start Indonesia')]");
    By xpathElementViewAllSeason = By.xpath("//div[@class='seasons__left-arrow']//img");

    //8th
    By xpathElementGoresanJejak = By.xpath("//div[@class='card__title']");
    By xpathElementTontonGoresanJejak = By.xpath("//a[@class='videoInfo__right-buttons-watchseason']");

    //9
    By xpathElementAfgan= By.xpath("//div[@class='card__title']");
    By getXpathElementAfganPart02= By.xpath("//h3[contains(text(),'Konser DEKADE AFGAN 2019 - Part 02/04')]");

    By xpathElementBahadur= By.xpath("//div[@class='card__title']");
    By xpathElementAddToLibrary = By.xpath("//div[contains(text(),'Add to library')]");
    By xpathElementRemoveFromLibrary = By.xpath("//div[contains(text(),'In Library')]");
    By xpathElementKoleksi = By.xpath("//a[@class='navbar_menu-item']");
    By xpathElementBahadurInLibrary = By.xpath("//img[@class='card__image-main']");
    By xpathElementBtnSlideshow = By.xpath("//body//button[3]");
    By xpathElementSlideshow = By.xpath("//div[@class='VueCarousel-slide custom-CarouselSlide VueCarousel-slide-active VueCarousel-slide-center']//a");
    By xpathElementDaihatsu = By.xpath("//div[@class='card__title'][contains(text(),'Daihatsu Indonesia Master 2019 Finals')]");
    By xpathElementDaihatsu2ndHighlight = By.xpath("//h3[contains(text(),'WD | Misaki Matsutomo/Ayaka Takahashi (JPN) vs Kim')]");
    By xpathElementBeranda = By.xpath("//a[@class='navbar_menu-item browseLink']");

    By xpathElementSeasonTheBigStart = By.xpath("//div[@class='seasons__left']");
    By xpathElement2ndSeasonTheBigStart = By.xpath("//div[@class='seasons__left-list'][contains(text(),'TBS Season 2')]");
    By xpathElementPause = By.xpath("//div[@id='player-custom-controller-container']");
    By xpathElementAkun = By.xpath("//a[contains(text(),'Akun')]");
    By xpathElementUbahProfile = By.xpath("//div[@class='profileDetails__container-header-editButton']");
    By xpathElementSaveProfile = By.xpath("//div[@class='profileDetails__container-header-saveButton']");
    By xpathElementFirstName = By.xpath("//body//div[@class='profileDetails__container-editInput']//div//div[1]//input[1]");
    By xpathElementLastName = By.xpath("//div[@class='profileDetails__container-editInput']//div[2]//input[1]");
    By xpathElementPengaturan = By.xpath("//a[contains(text(),'Pengaturan')]");
    By xpathElementKeluarDariPengaturan = By.xpath("//section[@class='container-logout']");
    By xpathElementOpenVideoOnBeranda = By.xpath("//div[contains(text(),'Blibli Indonesia Open 2018 Babak Penyisihan')]");

    public BliPlayHomePage(WebDriver webDriver){
        this.webDriver = webDriver;
    }

    public void openBlibliPlay() throws InterruptedException {
        webDriver.get("https://www.blibliplay.com");
        Thread.sleep(3000);
    }

    public void btnMasukClick(){
        WebElement btnMasuk = webDriver.findElement(xpathElementBtnMasuk);
        btnMasuk.click();
    }

    public void btnLogoutClick(){
        WebElement Logout = webDriver.findElement(xpathElementLogout);Logout.click();
        WebElement btnLogout = webDriver.findElement(xpathElementBtnLogout);btnLogout.click();
    }

    public void btnEmailClick(String searchKey) throws InterruptedException {
        Thread.sleep(2000);
        WebElement email = webDriver.findElement(xpathElementEmail);
        email.sendKeys(searchKey);
    }
    public void btnPasswordClick(String searchKey){
        WebElement pass = webDriver.findElement(xpathElementPass);
        pass.sendKeys(searchKey);
    }
    public void btnSubmitLoginClick(){
        WebElement submit = webDriver.findElement(xpathElementBtnSubmitLogin);
        submit.click();
    }

    public void searchVideoIcon(){
        WebElement searchTxBoxIcon = webDriver.findElement(getXpathElementTxtBoxIcon);
        searchTxBoxIcon.click();
    }

    public void searchVideo(String searchKey){
        WebElement searchTxBox = webDriver.findElement(xpathElementTxtBox);
        searchTxBox.sendKeys(searchKey);
    }

    public void enterKey(){
        WebElement searchTxBox = webDriver.findElement(xpathElementTxtBox);
        searchTxBox.sendKeys(Keys.ENTER);
    }

    public void downKey(){
        WebElement searchTxBox = webDriver.findElement(xpathElementTxtBox);
        searchTxBox.sendKeys(Keys.ARROW_DOWN);
    }

    public void upKey(){
        WebElement searchTxBox = webDriver.findElement(xpathElementTxtBox);
        searchTxBox.sendKeys(Keys.ARROW_UP);
    }

    // 1st Case
    public void btnTontonClick(){
        WebElement btnTonton = webDriver.findElement(xpathElementBtnTonton);
        btnTonton.click();
    }

    public void btnVideoClick(){
        WebElement btnVideo = webDriver.findElement(xpathElementBtnvideo); btnVideo.click();
    }

    public void btnTonton(){
        WebElement btnTonton = webDriver.findElement(xpathElementBtnTonton); btnTonton.click();
    }

    public void btnLikeClick(){
        WebElement BtnLike = webDriver.findElement(xpathElementBtnLike); BtnLike.click();
    }
    public void btnDislikeClick(){
        WebElement BtnDislike = webDriver.findElement(xpathElementBtnDislike); BtnDislike.click();
    }
    public void btnShareClick(){
        WebElement BtnShare = webDriver.findElement(xpathElementBtnShare); BtnShare.click();
    }
    public void btnCloseShareClick(){
        WebElement BtnCloseShare = webDriver.findElement(xpathElementCloseBtnShare); BtnCloseShare.click();
    }

    public void tentangBliPlayClick(){
        WebElement TentangBliPlay = webDriver.findElement(xpathElementTentangBliPlay); TentangBliPlay.click();
    }
    public void belanjaDiBlibliClick(){
        WebElement BelanjadiBlibli = webDriver.findElement(xpathElementBelanjadiBlibli); BelanjadiBlibli.click();
    }
    public void videoBahadurClick(){
        WebElement BahadurClick = webDriver.findElement(xpathElementBahadur); BahadurClick.click();
    }
    public void addToLibraryClick(){
        WebElement addToLibrary = webDriver.findElement(xpathElementAddToLibrary); addToLibrary.click();
    }
    public void removeFromLibraryClick(){
        WebElement removeFromLibrary = webDriver.findElement(xpathElementRemoveFromLibrary); removeFromLibrary.click();
    }
    public void openKoleksi(){
        WebElement koleksiclick = webDriver.findElement(xpathElementKoleksi); koleksiclick.click();
    }
    public void openBahadurInLibrary(){
        WebElement BahadurInLibrary = webDriver.findElement(xpathElementBahadurInLibrary); BahadurInLibrary.click();
    }
    public void openSlideshowVideo(){
        WebElement btnSlideshowClick = webDriver.findElement(xpathElementSlideshow); btnSlideshowClick.click();
        //WebElement slideshowClick = webDriver.findElement(xpathElementBtnSlideshow); slideshowClick.click();
    }
    public void openDaihatsuVideo(){
        WebElement btnOpenDaihatsu = webDriver.findElement(xpathElementDaihatsu); btnOpenDaihatsu.click();
    }
    public void openDaihatsu2ndHighlight(){
        WebElement OpenDaihatsu2nd = webDriver.findElement(xpathElementDaihatsu2ndHighlight); OpenDaihatsu2nd.click();
    }
    public void btnAkunClick(){
        WebElement OpenAkun = webDriver.findElement(xpathElementAkun); OpenAkun.click();
    }
    public void btnUbahProfile(){
        WebElement UbahProfile = webDriver.findElement(xpathElementUbahProfile); UbahProfile.click();
    }
    public void btnSaveProfile(){
        WebElement SaveProfile = webDriver.findElement(xpathElementSaveProfile); SaveProfile.click();
    }
    public void btnFirstNameClick(){
        WebElement email = webDriver.findElement(xpathElementFirstName);
        email.click();
    }
    public void btnLastNameClick(){
        WebElement email = webDriver.findElement(xpathElementLastName);
        email.click();
    }
    public void btnFillFirstName(String searchKey){
        WebElement email = webDriver.findElement(xpathElementFirstName);
        email.sendKeys(searchKey);
    }
    public void btnFillLastName(String searchKey){
        WebElement email = webDriver.findElement(xpathElementLastName);
        email.sendKeys(searchKey);
    }
    public void selectAllInFirstName(){
        String selectAll = Keys.chord(Keys.CONTROL, "a");
        webDriver.findElement(xpathElementFirstName).sendKeys(selectAll);
    }
    public void selectAllInLastName(){
        String selectAll = Keys.chord(Keys.CONTROL, "a");
        webDriver.findElement(xpathElementLastName).sendKeys(selectAll);
    }
    public void btnPengaturanClick(){
        WebElement pengaturanclick = webDriver.findElement(xpathElementPengaturan);
        pengaturanclick.click();
    }
    public void btnLogoutFromPengaturanClick(){
        WebElement pengaturanclick = webDriver.findElement(xpathElementKeluarDariPengaturan);
        pengaturanclick.click();
        WebElement btnLogout = webDriver.findElement(xpathElementBtnLogout);btnLogout.click();
    }
    public void openVideoOnBeranda(){
        WebElement videoOnBeranda = webDriver.findElement(xpathElementOpenVideoOnBeranda);
        videoOnBeranda.click();
    }

    public void pauseClick(){
        WebElement pause = webDriver.findElement(xpathElementPause); pause.click();
    }
    public void seasonTheBigStart(){
        WebElement clickSeasonTheBigStart = webDriver.findElement(xpathElementSeasonTheBigStart); clickSeasonTheBigStart.click();
    }
    public void secondSeasonTheBigStart(){
        WebElement click2ndSeasonTheBigStart = webDriver.findElement(xpathElement2ndSeasonTheBigStart); click2ndSeasonTheBigStart.click();
    }

    public void berandaClick(){
        WebElement clickBeranda = webDriver.findElement(xpathElementBeranda); clickBeranda.click();
    }

    // 7th Case
    public void theBigStartClick(){
        WebElement TheBigStart = webDriver.findElement(xpathElementTheBigStart); TheBigStart.click();
    }
    public void viewAllSeasonClick(){
        WebElement ViewAllSeason = webDriver.findElement(xpathElementViewAllSeason); ViewAllSeason.click();
    }
    public  void open6thcase() throws InterruptedException {
        WebElement TheBigStart = webDriver.findElement(xpathElementTheBigStart); TheBigStart.click();
        Thread.sleep(2000);
        WebElement ViewAllSeason = webDriver.findElement(xpathElementViewAllSeason); ViewAllSeason.click();
        Thread.sleep(5000);
    }

    // 8th Case
    public void tontonGoresanJejak(){
        WebElement kemeriahanBlibliClick = webDriver.findElement(xpathElementGoresanJejak); kemeriahanBlibliClick.click();
        WebElement tontonKemeriahanBlibliClick = webDriver.findElement(xpathElementTontonGoresanJejak); tontonKemeriahanBlibliClick.click();
    }

    //9
    public void afganClick(){
        WebElement afganClick = webDriver.findElement(xpathElementAfgan); afganClick.click();
    }

    public void afganPart02Click(){
        WebElement afganPart02Click = webDriver.findElement(getXpathElementAfganPart02); afganPart02Click.click();
    }


}
