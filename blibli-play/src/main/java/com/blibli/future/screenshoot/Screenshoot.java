package com.blibli.future.screenshoot;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;

import java.io.File;
import java.io.IOException;

public class Screenshoot {
    WebDriver webDriver;

    public Screenshoot(WebDriver webDriver) {
        this.webDriver = webDriver;
    }
     public void getScreenshoot(){
         TakesScreenshot screenshoot = ((TakesScreenshot)webDriver);
         File src = screenshoot.getScreenshotAs(OutputType.FILE);
         try{
             FileUtils.copyFile(src,new File(System.getProperty("user.dir") + "/Screenshot/" + System.currentTimeMillis() + ".png"));
         } catch(IOException e){
             e.printStackTrace();
         }
     }
}
