package com.blibli.future;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

import com.blibli.future.bliblipage.BliPlayHomePage;
import com.blibli.future.bliblipage.BliPlayProperties;
import com.blibli.future.bliblipage.BlibliHomePage;
import com.blibli.future.screenshoot.Screenshoot;
import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;
import com.relevantcodes.extentreports.model.ITest;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.apache.commons.io.FileUtils;
import org.junit.*;
import org.junit.runners.Parameterized;
import org.junit.*;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.opera.OperaDriver;
import org.openqa.selenium.opera.OperaOptions;
import org.openqa.selenium.support.ui.Sleeper;

import java.io.*;
import java.util.Properties;
import java.util.concurrent.TimeUnit;
/**
 * Unit test for simple App.
 */
public class AppTest
{
    WebDriver webDriver;
    static ExtentTest test;
    static ExtentReports report;

    BliPlayProperties bliPlayProperties = new BliPlayProperties();

    public static String capture(WebDriver driver) throws IOException {
        File srcFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
        File Dest = new File("src/../ErrImages/" + System.currentTimeMillis()
                + ".png");
        String errflpath = Dest.getAbsolutePath();
        FileUtils.copyFile(srcFile, Dest);
        return errflpath;
    }

   @BeforeClass
    public static void startTest(){
        report = new ExtentReports(System.getProperty("user.dir")+"\\ExtentReportBlibliPlay.html");
        report
                .addSystemInfo("Host Name", "Ivani Firous")
                .addSystemInfo("Environment", "Testing")
                .addSystemInfo("User Name","ivanifirous")
                .addSystemInfo("Tester Name","random");
    }

  @Before
    public void setup() throws Exception{
        String Browser = bliPlayProperties.getProperties("properties.browser");

        //Check if parameter passed is 'firefox'
        if(Browser.equalsIgnoreCase("firefox")){
            WebDriverManager.firefoxdriver().setup();
            webDriver = new FirefoxDriver();
        }
        //Check if parameter passed as 'chrome'
        else if(Browser.equalsIgnoreCase("chrome")){
            WebDriverManager.chromedriver().setup();
            webDriver = new ChromeDriver();
        }
        else{
            //If no browser passed throw exception
            throw new Exception("Browser is not correct");
        }
        webDriver.manage().window().maximize();
        webDriver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
    }

    @AfterClass
    public static void endTest(){
        report.endTest(test);
        report.flush();
    }

    @After
    public void endTesting()
    {
        webDriver.close();
    }

   // @Test
    public void runner() throws IOException, InterruptedException {
        LoginTest();
        LogoutFromIconAccount();
        LogoutFromSetting();
        SearchVideo();
        SearchVideoWithUnknownKey();
        SearchVideoWithTooLongKey();
        WatchVideoWithoutLoginFirst();
        WatchVideoWithLoginFirst();
        WatchVideoFromSlideshow();
        OpenVideoOnBeranda();
        OpenThe2ndHighlightVideo();
        LikeVideo();
        DislikeVideo();
        ShareVideo();
        AddToLibrary();
        RemoveFromLibrary();
        OpenLibrary();
        ChangeFirstNameOfAccount();
        ChangeLastNameOfAccount();
        OpenTentangBlibliplay();
    }

    //1st Testcase
    // Login
    @Test
    public void LoginTest () throws InterruptedException, IOException {
        test = report.startTest("Login Test");

        BliPlayHomePage bliPlayHomePage = new BliPlayHomePage(webDriver);
        BlibliHomePage blibliHomePage = new BlibliHomePage(webDriver);
        blibliHomePage.openHomePage();
        blibliHomePage.openBliPlay();

        String actual1TC1 = webDriver.findElement(By.xpath("//a[@class='navbar_menu-item browseLink exactActive active']")).getText();
        if (actual1TC1.equals("Beranda")) {
            test.log(LogStatus.PASS,"Buka BlibliPlay", test.addScreenCapture(capture(webDriver)) + "Berhasil membuka BlibliPlay");
        } else {
            test.log(LogStatus.FAIL,"Buka BlibliPlay", test.addScreenCapture(capture(webDriver)) + "Test Gagal");
        }

        bliPlayHomePage.btnMasukClick();
        bliPlayHomePage.btnEmailClick(bliPlayProperties.getProperties("properties.email"));
        bliPlayHomePage.btnPasswordClick(bliPlayProperties.getProperties("properties.password"));
        bliPlayHomePage.btnSubmitLoginClick();
        Thread.sleep(3000);

        String actual1TC2 = webDriver.findElement(By.xpath("//div[contains(text(),'Hi, cobacobabl...')]")).getText();
        if (actual1TC2.equals("Hi, cobacobabl...")) {
            test.log(LogStatus.PASS,"Login ke BlibliPlay", test.addScreenCapture(capture(webDriver)) + "Login Berhasil");
        } else {
            test.log(LogStatus.FAIL, "Login ke BlibliPlay",test.addScreenCapture(capture(webDriver)) + "Login Gagal");
        }

        // LOGOUT
        Actions builder = new Actions(webDriver);
        Action mouseOverHome = builder
                .moveToElement(webDriver.findElement(By.xpath("//div[contains(text(),'Hi, cobacobabl...')]")))
                .build();

        mouseOverHome.perform();

        bliPlayHomePage.btnLogoutClick();
    }

    //2nd Testcase
    // Logout from account
    @Test
    public void LogoutFromIconAccount () throws InterruptedException, IOException {
        test = report.startTest("Logout from icon account");

        BliPlayHomePage bliPlayHomePage = new BliPlayHomePage(webDriver);
        BlibliHomePage blibliHomePage = new BlibliHomePage(webDriver);
        blibliHomePage.openHomePage();
        blibliHomePage.openBliPlay();

        String actual15TC1 = webDriver.findElement(By.xpath("//a[@class='navbar_menu-item browseLink exactActive active']")).getText();
        if (actual15TC1.equals("Beranda")) {
            test.log(LogStatus.PASS,"Buka BlibliPlay", test.addScreenCapture(capture(webDriver)) + "Berhasil membuka BlibliPlay");
        } else {
            test.log(LogStatus.FAIL,"Buka BlibliPlay", test.addScreenCapture(capture(webDriver)) + "Test Gagal");
        }

        bliPlayHomePage.btnMasukClick();
        bliPlayHomePage.btnEmailClick(bliPlayProperties.getProperties("properties.email"));
        bliPlayHomePage.btnPasswordClick(bliPlayProperties.getProperties("properties.password"));
        bliPlayHomePage.btnSubmitLoginClick();
        Thread.sleep(3000);

        String actual15TC2 = webDriver.findElement(By.xpath("//div[contains(text(),'Hi, cobacobabl...')]")).getText();
        if (actual15TC2.equals("Hi, cobacobabl...")) {
            test.log(LogStatus.PASS,"Login ke BlibliPlay", test.addScreenCapture(capture(webDriver)) + "Login Berhasil");
        } else {
            test.log(LogStatus.FAIL, "Login ke BlibliPlay",test.addScreenCapture(capture(webDriver)) + "Login Gagal");
        }

        // LOGOUT
        Actions builder = new Actions(webDriver);
        Action mouseOverHome = builder
                .moveToElement(webDriver.findElement(By.xpath("//div[contains(text(),'Hi, cobacobabl...')]")))
                .build();

        mouseOverHome.perform();

        String actual15TC3 = webDriver.findElement(By.xpath("//div[@class='navbar_user-loggedin-details-list']")).getText();
        if (actual15TC3.equals("Keluar")) {
            test.log(LogStatus.PASS,"Logout dari BlibliPlay", test.addScreenCapture(capture(webDriver)) + "Logout Berhasil");
        } else {
            test.log(LogStatus.FAIL, "Logout dari BlibliPlay",test.addScreenCapture(capture(webDriver)) + "Logout Gagal");
        }

        bliPlayHomePage.btnLogoutClick();
        Thread.sleep(1000);

        String actual15TC4 = webDriver.findElement(By.xpath("//a[@class='navbar_menu-item browseLink exactActive active']")).getText();
        if (actual15TC4.equals("Beranda")) {
            test.log(LogStatus.PASS,"Logout dari BlibliPlay", test.addScreenCapture(capture(webDriver)) + "Logout Berhasil");
        } else {
            test.log(LogStatus.FAIL,"Logout dari BlibliPlay", test.addScreenCapture(capture(webDriver)) + "Logout Gagal");
        }

        Thread.sleep(2000);

    }

    //3rd Testcase
    // Logout from settings
    @Test
    public void LogoutFromSetting () throws InterruptedException, IOException {
        test = report.startTest("Logout from setting");

        BliPlayHomePage bliPlayHomePage = new BliPlayHomePage(webDriver);
        BlibliHomePage blibliHomePage = new BlibliHomePage(webDriver);
        blibliHomePage.openHomePage();
        blibliHomePage.openBliPlay();

        String actual16TC1 = webDriver.findElement(By.xpath("//a[@class='navbar_menu-item browseLink exactActive active']")).getText();
        if (actual16TC1.equals("Beranda")) {
            test.log(LogStatus.PASS,"Buka BlibliPlay", test.addScreenCapture(capture(webDriver)) + "Berhasil membuka BlibliPlay");
        } else {
            test.log(LogStatus.FAIL,"Buka BlibliPlay", test.addScreenCapture(capture(webDriver)) + "Test Gagal");
        }

        bliPlayHomePage.btnMasukClick();
        bliPlayHomePage.btnEmailClick(bliPlayProperties.getProperties("properties.email"));
        bliPlayHomePage.btnPasswordClick(bliPlayProperties.getProperties("properties.password"));
        bliPlayHomePage.btnSubmitLoginClick();
        Thread.sleep(3000);

        String actual16TC2 = webDriver.findElement(By.xpath("//div[contains(text(),'Hi, cobacobabl...')]")).getText();
        if (actual16TC2.equals("Hi, cobacobabl...")) {
            test.log(LogStatus.PASS,"Login ke BlibliPlay", test.addScreenCapture(capture(webDriver)) + "Login Berhasil");
        } else {
            test.log(LogStatus.FAIL, "Login ke BlibliPlay",test.addScreenCapture(capture(webDriver)) + "Login Gagal");
        }

        // LOGOUT
        Actions builder = new Actions(webDriver);
        Action mouseOverHome = builder
                .moveToElement(webDriver.findElement(By.xpath("//div[contains(text(),'Hi, cobacobabl...')]")))
                .build();

        mouseOverHome.perform();

        String actual16TC3 = webDriver.findElement(By.xpath("//div[@class='navbar_user-loggedin-details-list']")).getText();
        if (actual16TC3.equals("Keluar")) {
            test.log(LogStatus.PASS,"Menekan icon akun", test.addScreenCapture(capture(webDriver)) + "Berhasil menekan icon akun");
        } else {
            test.log(LogStatus.FAIL, "Menekan icon akun",test.addScreenCapture(capture(webDriver)) + "Test gagal");
        }

        bliPlayHomePage.btnPengaturanClick();
        Thread.sleep(1000);

        String actual16TC4 = webDriver.findElement(By.xpath("//div[@class='settings__details-heading']")).getText();
        if (actual16TC4.equals("Pengaturan")) {
            test.log(LogStatus.PASS,"Membuka Pengaturan", test.addScreenCapture(capture(webDriver)) + "Berhasil Membuka Pengaturan");
        } else {
            test.log(LogStatus.FAIL,"Membuka Pengaturan", test.addScreenCapture(capture(webDriver)) + "Test Gagal");
        }
        Thread.sleep(1000);

        bliPlayHomePage.btnLogoutFromPengaturanClick();

        String actual16TC5 = webDriver.findElement(By.xpath("//a[@class='navbar_menu-item browseLink exactActive active']")).getText();
        if (actual16TC5.equals("Beranda")) {
            test.log(LogStatus.PASS,"Logout dari BlibliPlay", test.addScreenCapture(capture(webDriver)) + "Logout Berhasil");
        } else {
            test.log(LogStatus.FAIL,"Logout dari BlibliPlay", test.addScreenCapture(capture(webDriver)) + "Logout Gagal");
        }
        Thread.sleep(1000);
    }

    // 4th Testcase
    // Search Video
    @Test
    public void SearchVideo () throws InterruptedException, IOException {
        test = report.startTest("Search video");
        BliPlayHomePage bliPlayHomePage = new BliPlayHomePage(webDriver);
        bliPlayHomePage.openBlibliPlay();

        String actual2TC1 = webDriver.findElement(By.xpath("//a[@class='navbar_menu-item browseLink exactActive active']")).getText();
        if (actual2TC1.equals("Beranda")) {
            test.log(LogStatus.PASS,"Buka BlibliPlay", test.addScreenCapture(capture(webDriver)) + "Berhasil membuka BlibliPlay");
        } else {
            test.log(LogStatus.FAIL, "Buka BlibliPlay",test.addScreenCapture(capture(webDriver)) + "Test Gagal");
        }

        bliPlayHomePage.searchVideoIcon();
        bliPlayHomePage.searchVideo(bliPlayProperties.getProperties("properties.key"));
        bliPlayHomePage.enterKey();
        Thread.sleep(2000);

        String actual2TC2 = webDriver.findElement(By.xpath("//div[contains(text(),'Blibli Indonesia Open 2018 Finals')]")).getText();
        if (actual2TC2.contains("Blibli Indonesia Open 2018 Finals")) {
            test.log(LogStatus.PASS, "Cari video Blibli Indonesia Open 2018",test.addScreenCapture(capture(webDriver)) + "Berhasil mencari video");
        } else {
            test.log(LogStatus.FAIL, "Cari video Blibli Indonesia Open 2018",test.addScreenCapture(capture(webDriver)) + "Test Gagal");
        }
    }

    // 5th Testcase
    // Incorrect Key
    @Test
    public void SearchVideoWithUnknownKey() throws InterruptedException, IOException {
        test = report.startTest("Search video with unknown key");
        BliPlayHomePage bliPlayHomePage = new BliPlayHomePage(webDriver);

        bliPlayHomePage.openBlibliPlay();
        Thread.sleep(3000);

        String actual9TC1 = webDriver.findElement(By.xpath("//a[@class='navbar_menu-item browseLink exactActive active']")).getText();
        if (actual9TC1.equals("Beranda")) {
            test.log(LogStatus.PASS,"Buka BlibliPlay", test.addScreenCapture(capture(webDriver)) + "Berhasil membuka BlibliPlay");
        } else {
            test.log(LogStatus.FAIL, "Buka BlibliPlay",test.addScreenCapture(capture(webDriver)) + "Test Gagal");
        }

        bliPlayHomePage.searchVideoIcon();
        bliPlayHomePage.searchVideo(bliPlayProperties.getProperties("properties.incorrectKey"));
        Thread.sleep(1000);
        bliPlayHomePage.enterKey();
        Thread.sleep(1000);
        String actual9TC2 = webDriver.findElement(By.xpath("//div[@class='search__container-resultsCount']")).getText();
        if(actual9TC2.equals("0 hasil pencarian")) {
            test.log(LogStatus.PASS, "Mencari video dengan kata kunci yang salah",test.addScreenCapture(capture(webDriver)) +"Berhasil menampilkan hasil pencarian");
        } else
        {
            test.log(LogStatus.FAIL,"Mencari video dengan kata kunci yang salah",test.addScreenCapture(capture(webDriver)) + "Test Gagal");
        }
    }

    // 6th Testcase
    // Too long character
    @Test
    public void SearchVideoWithTooLongKey() throws InterruptedException, IOException {
        test = report.startTest("Search video with too long key");
        BliPlayHomePage bliPlayHomePage = new BliPlayHomePage(webDriver);

        bliPlayHomePage.openBlibliPlay();
        Thread.sleep(3000);

        String actual10TC1 = webDriver.findElement(By.xpath("//a[@class='navbar_menu-item browseLink exactActive active']")).getText();
        if (actual10TC1.equals("Beranda")) {
            test.log(LogStatus.PASS,"Buka BlibliPlay", test.addScreenCapture(capture(webDriver)) + "Berhasil membuka BlibliPlay");
        } else {
            test.log(LogStatus.FAIL, "Buka BlibliPlay",test.addScreenCapture(capture(webDriver)) + "Test Gagal");
        }

        bliPlayHomePage.searchVideoIcon();
        bliPlayHomePage.searchVideo(bliPlayProperties.getProperties("properties.paragraph"));
        Thread.sleep(1000);
        bliPlayHomePage.enterKey();
        Thread.sleep(1000);
        String actual10TC2 = webDriver.findElement(By.xpath("//div[@class='noConnection-heading']")).getText();
        if(actual10TC2.equals("Blibliplay Lagi Dibanjiri Penonton")) {
            test.log(LogStatus.PASS, "Mencari video dengan kata kunci terlalu panjang (302 kata)",test.addScreenCapture(capture(webDriver)) +"Berhasil menampilkan hasil pencarian");
        } else
        {
            test.log(LogStatus.FAIL,"Mencari video dengan kata kunci terlalu panjang (302 kata)",test.addScreenCapture(capture(webDriver)) + "Test Gagal");
        }
    }

    // 7th Testcase
    // Nonton video + tidak login
    @Test
    public void WatchVideoWithoutLoginFirst () throws InterruptedException, IOException {
        test = report.startTest("Watch video without login first");
        BliPlayHomePage bliPlayHomePage = new BliPlayHomePage(webDriver);
        bliPlayHomePage.openBlibliPlay();

        String actual2TC1 = webDriver.findElement(By.xpath("//a[@class='navbar_menu-item browseLink exactActive active']")).getText();
        if (actual2TC1.equals("Beranda")) {
            test.log(LogStatus.PASS,"Buka BlibliPlay", test.addScreenCapture(capture(webDriver)) + "Berhasil membuka BlibliPlay");
        } else {
            test.log(LogStatus.FAIL, "Buka BlibliPlay",test.addScreenCapture(capture(webDriver)) + "Test Gagal");
        }

        bliPlayHomePage.searchVideoIcon();
        bliPlayHomePage.searchVideo(bliPlayProperties.getProperties("properties.key"));
        bliPlayHomePage.enterKey();
        Thread.sleep(2000);

        String actual2TC2 = webDriver.findElement(By.xpath("//div[contains(text(),'Blibli Indonesia Open 2018 Finals')]")).getText();
        if (actual2TC2.contains("Blibli Indonesia Open 2018 Finals")) {
            test.log(LogStatus.PASS, "Cari video Blibli Indonesia Open 2018",test.addScreenCapture(capture(webDriver)) + "Berhasil mencari video");
        } else {
            test.log(LogStatus.FAIL, "Cari video Blibli Indonesia Open 2018",test.addScreenCapture(capture(webDriver)) + "Test Gagal");
        }

        // 2nd case
        Thread.sleep(5000);
        bliPlayHomePage.btnVideoClick();
        bliPlayHomePage.btnTonton();
        Thread.sleep(2000);

        String actual2TC3 = webDriver.findElement(By.xpath("//div[@class='content__description']")).getText();
        if (actual2TC3.equals("Masuk ke akun Blibli Anda untuk menikmati Blibliplay")) {
            test.log(LogStatus.PASS,"Meminta untuk Login",test.addScreenCapture(capture(webDriver)) + "Berhasil meminta untuk login");
        } else {
            test.log(LogStatus.FAIL, "Meminta untuk Login",test.addScreenCapture(capture(webDriver)) + "Test Gagal");
        }


        bliPlayHomePage.btnEmailClick(bliPlayProperties.getProperties("properties.email"));
        bliPlayHomePage.btnPasswordClick(bliPlayProperties.getProperties("properties.password"));
        bliPlayHomePage.btnSubmitLoginClick();
        Thread.sleep(1000);

        String actual2TC4 = webDriver.findElement(By.xpath("//div[contains(text(),'Hi, cobacobabl...')]")).getText();
        if (actual2TC4.equals("Hi, cobacobabl...")) {
            test.log(LogStatus.PASS, "Login", test.addScreenCapture(capture(webDriver)) + "Berhasil login");
        } else {
            test.log(LogStatus.FAIL, "Login", test.addScreenCapture(capture(webDriver)) + "Login Gagal");
        }

        Actions builder = new Actions(webDriver);
        Action mouseOverHome = builder
                .moveToElement(webDriver.findElement(By.xpath("//div[contains(text(),'Hi, cobacobabl...')]")))
                .build();

        mouseOverHome.perform();

        bliPlayHomePage.btnLogoutClick();

    }

    //8th Testcase
    // Nonton video + login terlebih dahulu + Buka Blibli.com
    @Test
    public void WatchVideoWithLoginFirst () throws InterruptedException, IOException {
        test = report.startTest("Watch video with login first");

        BliPlayHomePage bliPlayHomePage = new BliPlayHomePage(webDriver);
        BlibliHomePage blibliHomePage = new BlibliHomePage(webDriver);
        blibliHomePage.openHomePage();
        blibliHomePage.openBliPlay();

        String actual1TC1 = webDriver.findElement(By.xpath("//a[@class='navbar_menu-item browseLink exactActive active']")).getText();
        if (actual1TC1.equals("Beranda")) {
            test.log(LogStatus.PASS,"Buka BlibliPlay", test.addScreenCapture(capture(webDriver)) + "Berhasil membuka BlibliPlay");
        } else {
            test.log(LogStatus.FAIL,"Buka BlibliPlay", test.addScreenCapture(capture(webDriver)) + "Test Gagal");
        }

        bliPlayHomePage.btnMasukClick();
        bliPlayHomePage.btnEmailClick(bliPlayProperties.getProperties("properties.email"));
        bliPlayHomePage.btnPasswordClick(bliPlayProperties.getProperties("properties.password"));
        bliPlayHomePage.btnSubmitLoginClick();
        Thread.sleep(3000);

        String actual1TC2 = webDriver.findElement(By.xpath("//div[contains(text(),'Hi, cobacobabl...')]")).getText();
        if (actual1TC2.equals("Hi, cobacobabl...")) {
            test.log(LogStatus.PASS,"Login ke BlibliPlay", test.addScreenCapture(capture(webDriver)) + "Login Berhasil");
        } else {
            test.log(LogStatus.FAIL, "Login ke BlibliPlay",test.addScreenCapture(capture(webDriver)) + "Login Gagal");
        }

        bliPlayHomePage.searchVideoIcon();
        bliPlayHomePage.searchVideo(bliPlayProperties.getProperties("properties.key"));
        bliPlayHomePage.enterKey();
        Thread.sleep(2000);
        bliPlayHomePage.btnVideoClick();
        Thread.sleep(2000);

        String actual1TC3 = webDriver.findElement(By.xpath("//div[contains(text(),'Blibli Indonesia Open 2018 Finals')]")).getText();
        if (actual1TC3.contains("Blibli Indonesia Open 2018")) {
            test.log(LogStatus.PASS, "Buka Video Blibli Indonesia Open 2018",test.addScreenCapture(capture(webDriver)) + "Berhasil membuka video");
        } else {
            test.log(LogStatus.FAIL,"Buka Video Blibli Indonesia Open 2018", test.addScreenCapture(capture(webDriver)) + "Test Gagal");
        }

        bliPlayHomePage.btnTontonClick();
        Thread.sleep(4000);

        String actual1TC4 = webDriver.findElement(By.xpath("//div[contains(text(),'MD | GIDEON - SUKAMULJO (INA) VS INOUE - KANEKO (JPN)')]")).getText();
        if (actual1TC4.equals("MD | GIDEON - SUKAMULJO (INA) VS INOUE - KANEKO (JPN)")) {
            test.log(LogStatus.PASS, "Tonton Video Blibli Indonesia Open 2018",test.addScreenCapture(capture(webDriver)) + "Video berhasil ditonton");
        } else {
            test.log(LogStatus.FAIL, "Tonton Video Blibli Indonesia Open 2018",test.addScreenCapture(capture(webDriver)) + "Test Gagal");
        }

        // LOGOUT
        Actions builder = new Actions(webDriver);
        Action mouseOverHome = builder
                .moveToElement(webDriver.findElement(By.xpath("//div[contains(text(),'Hi, cobacobabl...')]")))
                .build();

        mouseOverHome.perform();

        bliPlayHomePage.btnLogoutClick();
    }

    // 9th Test
    // Tonton dari slideshow
    @Test
    public void WatchVideoFromSlideshow () throws InterruptedException, IOException {
        test = report.startTest("Watch video from slideshow");
        BliPlayHomePage bliPlayHomePage = new BliPlayHomePage(webDriver);
        bliPlayHomePage.openBlibliPlay();

        String actual8TC1 = webDriver.findElement(By.xpath("//a[@class='navbar_menu-item browseLink exactActive active']")).getText();
        if (actual8TC1.equals("Beranda")) {
            test.log(LogStatus.PASS,"Buka BlibliPlay", test.addScreenCapture(capture(webDriver)) + "Berhasil membuka BlibliPlay");
        } else {
            test.log(LogStatus.FAIL, "Buka BlibliPlay",test.addScreenCapture(capture(webDriver)) + "Test Gagal");
        }

        bliPlayHomePage.btnMasukClick();
        bliPlayHomePage.btnEmailClick(bliPlayProperties.getProperties("properties.email"));
        bliPlayHomePage.btnPasswordClick(bliPlayProperties.getProperties("properties.password"));
        bliPlayHomePage.btnSubmitLoginClick();
        Thread.sleep(3000);

        String actual8TC2 = webDriver.findElement(By.xpath("//div[contains(text(),'Hi, cobacobabl...')]")).getText();
        if (actual8TC2.equals("Hi, cobacobabl...")) {
            test.log(LogStatus.PASS,"Login ke BlibliPlay", test.addScreenCapture(capture(webDriver)) + "Login Berhasil");
        } else {
            test.log(LogStatus.FAIL, "Login ke BlibliPlay",test.addScreenCapture(capture(webDriver)) + "Login Gagal");
        }

        bliPlayHomePage.openSlideshowVideo();

        String actual8TC3 = webDriver.findElement(By.xpath("//div[@class='videoInfo__right-title']")).getText();
        if (actual8TC3.equals("Mereka Yang Hidupkan Mimpi")) {
            test.log(LogStatus.PASS,"Membuka detail video", test.addScreenCapture(capture(webDriver)) + "Berhasil membuka detail video");
        } else if (actual8TC3.equals("HERITAGE")) {
            test.log(LogStatus.PASS,"Membuka detail video", test.addScreenCapture(capture(webDriver)) + "Berhasil membuka detail video");
        } else if (actual8TC3.equals("Konser DEKADE AFGAN")) {
            test.log(LogStatus.PASS,"Membuka detail video", test.addScreenCapture(capture(webDriver)) + "Berhasil membuka detail video");
        } else {
            test.log(LogStatus.FAIL, "Membuka detail video",test.addScreenCapture(capture(webDriver)) + "Test Gagal");
        }

        bliPlayHomePage.btnTontonClick();
        Thread.sleep(4000);
        String actual8TC4 = webDriver.findElement(By.xpath("//div[@class='videoInfo-title']")).getText();
        if (actual8TC4.equals("TRAILER")) {
            test.log(LogStatus.PASS,"Menonton video", test.addScreenCapture(capture(webDriver)) + "Berhasil menonton video");
        } else if (actual8TC4.equals("Pasola")) {
            test.log(LogStatus.PASS,"Menonton video", test.addScreenCapture(capture(webDriver)) + "Berhasil menonton video");
        } else if (actual8TC4.equals("Konser DEKADE AFGAN 2019 - Part 04/04")) {
            test.log(LogStatus.PASS,"Menonton video", test.addScreenCapture(capture(webDriver)) + "Berhasil menonton video");
        } else {
            test.log(LogStatus.FAIL, "Menonton video",test.addScreenCapture(capture(webDriver)) + "Test Gagal");
        }

        Actions builder = new Actions(webDriver);
        Action mouseOverHome = builder
                .moveToElement(webDriver.findElement(By.xpath("//div[contains(text(),'Hi, cobacobabl...')]")))
                .build();

        mouseOverHome.perform();

        bliPlayHomePage.btnLogoutClick();
    }

    // 10th Testcase
    // Open video on beranda
    @Test
    public void OpenVideoOnBeranda () throws InterruptedException, IOException {
        test = report.startTest("Open video on Beranda");
        BliPlayHomePage bliPlayHomePage = new BliPlayHomePage(webDriver);
        bliPlayHomePage.openBlibliPlay();

        String actual20TC1 = webDriver.findElement(By.xpath("//a[@class='navbar_menu-item browseLink exactActive active']")).getText();
        if (actual20TC1.equals("Beranda")) {
            test.log(LogStatus.PASS,"Buka BlibliPlay", test.addScreenCapture(capture(webDriver)) + "Berhasil membuka BlibliPlay");
        } else {
            test.log(LogStatus.FAIL, "Buka BlibliPlay",test.addScreenCapture(capture(webDriver)) + "Test Gagal");
        }

        bliPlayHomePage.btnMasukClick();
        bliPlayHomePage.btnEmailClick(bliPlayProperties.getProperties("properties.email"));
        bliPlayHomePage.btnPasswordClick(bliPlayProperties.getProperties("properties.password"));
        bliPlayHomePage.btnSubmitLoginClick();
        Thread.sleep(1000);

        String actual20TC2 = webDriver.findElement(By.xpath("//div[contains(text(),'Hi, cobacobabl...')]")).getText();
        if (actual20TC2.equals("Hi, cobacobabl...")) {
            test.log(LogStatus.PASS, "Login", test.addScreenCapture(capture(webDriver)) + "Berhasil login");
        } else {
            test.log(LogStatus.FAIL, "Login", test.addScreenCapture(capture(webDriver)) + "Login Gagal");
        }

        bliPlayHomePage.openVideoOnBeranda();
        String actual20TC3 = webDriver.findElement(By.xpath("//div[@class='videoInfo__right-title']")).getText();
        if (actual20TC3.equals("Blibli Indonesia Open 2018 Babak Penyisihan")) {
            test.log(LogStatus.PASS, "Membuka video dari beranda", test.addScreenCapture(capture(webDriver)) + "Berhasil membuka video dari beranda");
        } else {
            test.log(LogStatus.FAIL, "Membuka video dari beranda", test.addScreenCapture(capture(webDriver)) + "Test Gagal");
        }

        Actions builder = new Actions(webDriver);
        Action mouseOverHome = builder
                .moveToElement(webDriver.findElement(By.xpath("//div[contains(text(),'Hi, cobacobabl...')]")))
                .build();

        mouseOverHome.perform();

        bliPlayHomePage.btnLogoutClick();

    }

    // 11th Test
    // Menonton video di highlight berbeda
    @Test
    public void OpenThe2ndHighlightVideo () throws InterruptedException, IOException {
        test = report.startTest("Open the 2nd highlight video");
        BliPlayHomePage bliPlayHomePage = new BliPlayHomePage(webDriver);
        bliPlayHomePage.openBlibliPlay();

        String actual11TC1 = webDriver.findElement(By.xpath("//a[@class='navbar_menu-item browseLink exactActive active']")).getText();
        if (actual11TC1.equals("Beranda")) {
            test.log(LogStatus.PASS,"Buka BlibliPlay", test.addScreenCapture(capture(webDriver)) + "Berhasil membuka BlibliPlay");
        } else {
            test.log(LogStatus.FAIL,"Buka BlibliPlay", test.addScreenCapture(capture(webDriver)) + "Test Gagal");
        }

        bliPlayHomePage.btnMasukClick();
        bliPlayHomePage.btnEmailClick(bliPlayProperties.getProperties("properties.email"));
        bliPlayHomePage.btnPasswordClick(bliPlayProperties.getProperties("properties.password"));
        bliPlayHomePage.btnSubmitLoginClick();
        Thread.sleep(3000);

        String actual11TC2 = webDriver.findElement(By.xpath("//div[contains(text(),'Hi, cobacobabl...')]")).getText();
        if (actual11TC2.equals("Hi, cobacobabl...")) {
            test.log(LogStatus.PASS,"Login ke BlibliPlay", test.addScreenCapture(capture(webDriver)) + "Login Berhasil");
        } else {
            test.log(LogStatus.FAIL, "Login ke BlibliPlay",test.addScreenCapture(capture(webDriver)) + "Login Gagal");
        }

        bliPlayHomePage.searchVideoIcon();
        bliPlayHomePage.searchVideo(bliPlayProperties.getProperties("properties.daihatsu"));
        bliPlayHomePage.enterKey();
        Thread.sleep(2000);

        bliPlayHomePage.openDaihatsuVideo();
        String actual11TC3 = webDriver.findElement(By.xpath("//div[@class='card__title'][contains(text(),'Daihatsu Indonesia Master 2019 Finals')]")).getText();
        if (actual11TC3.contains("Daihatsu Indonesia Master 2019 Finals")) {
            test.log(LogStatus.PASS, "Cari video Daihatsu Indonesia Master 2019 Finals",test.addScreenCapture(capture(webDriver)) + "Berhasil mencari video");
        } else {
            test.log(LogStatus.FAIL, "Cari video Daihatsu Indonesia Master 2019 Finals",test.addScreenCapture(capture(webDriver)) + "Test Gagal");
        }

        bliPlayHomePage.openDaihatsu2ndHighlight();
        Thread.sleep(4000);
        String actual11TC4 = webDriver.findElement(By.xpath("//div[@class='videoInfo-title']")).getText();
        if (actual11TC4.contains("WD | Misaki Matsutomo/Ayaka Takahashi (JPN) vs Kim So Yeong/Kong Hee Yong (KOR)")) {
            test.log(LogStatus.PASS, "Menonton Daihatsu Indonesia Master 2019 Finals highlight 2",test.addScreenCapture(capture(webDriver)) + "Berhasil menonton video");
        } else {
            test.log(LogStatus.FAIL, "Menonton Daihatsu Indonesia Master 2019 Finals highlight 2",test.addScreenCapture(capture(webDriver)) + "Test Gagal");
        }

        Actions builder = new Actions(webDriver);
        Action mouseOverHome = builder
                .moveToElement(webDriver.findElement(By.xpath("//div[contains(text(),'Hi, cobacobabl...')]")))
                .build();

        mouseOverHome.perform();

        bliPlayHomePage.btnLogoutClick();
    }

    // 12th Test
    // Like video
    @Test
    public void LikeVideo () throws InterruptedException, IOException {
        test = report.startTest("Like video");
        BliPlayHomePage bliPlayHomePage = new BliPlayHomePage(webDriver);
        bliPlayHomePage.openBlibliPlay();

        String actual3TC1 = webDriver.findElement(By.xpath("//a[@class='navbar_menu-item browseLink exactActive active']")).getText();
        if (actual3TC1.equals("Beranda")) {
            test.log(LogStatus.PASS,"Buka BlibliPlay", test.addScreenCapture(capture(webDriver)) + "Berhasil membuka BlibliPlay");
        } else {
            test.log(LogStatus.FAIL, "Buka BlibliPlay",test.addScreenCapture(capture(webDriver)) + "Test Gagal");
        }

        bliPlayHomePage.searchVideoIcon();
        bliPlayHomePage.searchVideo(bliPlayProperties.getProperties("properties.key"));
        bliPlayHomePage.enterKey();
        Thread.sleep(2000);

        String actual3TC2 = webDriver.findElement(By.xpath("//div[contains(text(),'Blibli Indonesia Open 2018 Finals')]")).getText();
        if (actual3TC2.contains("Blibli Indonesia Open 2018 Finals")) {
            test.log(LogStatus.PASS, "Cari video Blibli Indonesia Open 2018",test.addScreenCapture(capture(webDriver)) + "Berhasil mencari video");
        } else {
            test.log(LogStatus.FAIL, "Cari video Blibli Indonesia Open 2018",test.addScreenCapture(capture(webDriver)) + "Test Gagal");
        }

        Thread.sleep(2000);
        bliPlayHomePage.btnVideoClick();
        bliPlayHomePage.btnMasukClick();
        Thread.sleep(2000);

        String actual3TC3 = webDriver.findElement(By.xpath("//div[@class='content__description']")).getText();
        if (actual3TC3.equals("Masuk ke akun Blibli Anda untuk menikmati Blibliplay")) {
            test.log(LogStatus.PASS,"Meminta untuk Login",test.addScreenCapture(capture(webDriver)) + "Berhasil meminta untuk login");
        } else {
            test.log(LogStatus.FAIL, "Meminta untuk Login",test.addScreenCapture(capture(webDriver)) + "Test Gagal");
        }

        bliPlayHomePage.btnEmailClick(bliPlayProperties.getProperties("properties.email"));
        bliPlayHomePage.btnPasswordClick(bliPlayProperties.getProperties("properties.password"));
        bliPlayHomePage.btnSubmitLoginClick();
        Thread.sleep(2000);

        String actual3TC4 = webDriver.findElement(By.xpath("//div[contains(text(),'Hi, cobacobabl...')]")).getText();
        if (actual3TC4.equals("Hi, cobacobabl...")) {
            test.log(LogStatus.PASS, "Login",test.addScreenCapture(capture(webDriver)) + "Login has been successful");
        } else {
            test.log(LogStatus.FAIL, "Login",test.addScreenCapture(capture(webDriver)) + "Login Failed");
        }

        Thread.sleep(2000);
        bliPlayHomePage.btnLikeClick();
        Thread.sleep(2000);

        String actual3TC5 = webDriver.findElement(By.xpath("//div[contains(text(),'Blibli Indonesia Open 2018 Finals')]")).getText();
        if (actual3TC5.contains("Blibli Indonesia Open 2018")) {
            test.log(LogStatus.PASS, "Like video",test.addScreenCapture(capture(webDriver)) + "Berhasil like video");
        } else {
            test.log(LogStatus.FAIL, "Like video",test.addScreenCapture(capture(webDriver)) + "Test Gagal");
        }

        Actions builder = new Actions(webDriver);
        Action mouseOverHome = builder
                .moveToElement(webDriver.findElement(By.xpath("//div[contains(text(),'Hi, cobacobabl...')]")))
                .build();

        mouseOverHome.perform();

        bliPlayHomePage.btnLogoutClick();
    }

    // 13th Test
    // Dislike video
    @Test
    public void DislikeVideo () throws InterruptedException, IOException {
        test = report.startTest("Dislike video");
        BliPlayHomePage bliPlayHomePage = new BliPlayHomePage(webDriver);
        bliPlayHomePage.openBlibliPlay();

        String actual4TC1 = webDriver.findElement(By.xpath("//a[@class='navbar_menu-item browseLink exactActive active']")).getText();
        if (actual4TC1.equals("Beranda")) {
            test.log(LogStatus.PASS,"Buka BlibliPlay", test.addScreenCapture(capture(webDriver)) + "Berhasil membuka BlibliPlay");
        } else {
            test.log(LogStatus.FAIL, "Buka BlibliPlay",test.addScreenCapture(capture(webDriver)) + "Test Gagal");
        }

        bliPlayHomePage.searchVideoIcon();
        bliPlayHomePage.searchVideo(bliPlayProperties.getProperties("properties.key"));
        bliPlayHomePage.enterKey();
        Thread.sleep(2000);

        String actual4TC2 = webDriver.findElement(By.xpath("//div[contains(text(),'Blibli Indonesia Open 2018 Finals')]")).getText();
        if (actual4TC2.contains("Blibli Indonesia Open 2018 Finals")) {
            test.log(LogStatus.PASS, "Cari video Blibli Indonesia Open 2018",test.addScreenCapture(capture(webDriver)) + "Berhasil mencari video");
        } else {
            test.log(LogStatus.FAIL, "Cari video Blibli Indonesia Open 2018",test.addScreenCapture(capture(webDriver)) + "Test Gagal");
        }

        Thread.sleep(2000);
        bliPlayHomePage.btnVideoClick();
        bliPlayHomePage.btnMasukClick();
        Thread.sleep(2000);

        String actual4TC3 = webDriver.findElement(By.xpath("//div[@class='content__description']")).getText();
        if (actual4TC3.equals("Masuk ke akun Blibli Anda untuk menikmati Blibliplay")) {
            test.log(LogStatus.PASS,"Meminta untuk Login",test.addScreenCapture(capture(webDriver)) + "Berhasil meminta untuk login");
        } else {
            test.log(LogStatus.FAIL, "Meminta untuk Login",test.addScreenCapture(capture(webDriver)) + "Test Gagal");
        }

        bliPlayHomePage.btnEmailClick(bliPlayProperties.getProperties("properties.email"));
        bliPlayHomePage.btnPasswordClick(bliPlayProperties.getProperties("properties.password"));
        bliPlayHomePage.btnSubmitLoginClick();
        Thread.sleep(2000);

        String actual4TC4 = webDriver.findElement(By.xpath("//div[contains(text(),'Hi, cobacobabl...')]")).getText();
        if (actual4TC4.equals("Hi, cobacobabl...")) {
            test.log(LogStatus.PASS, "Login",test.addScreenCapture(capture(webDriver)) + "Berhasil login");
        } else {
            test.log(LogStatus.FAIL, "Login",test.addScreenCapture(capture(webDriver)) + "Login Gagal");
        }

        Thread.sleep(2000);
        bliPlayHomePage.btnDislikeClick();
        Thread.sleep(2000);

        String actual4TC5 = webDriver.findElement(By.xpath("//div[contains(text(),'Blibli Indonesia Open 2018 Finals')]")).getText();
        if (actual4TC5.contains("Blibli Indonesia Open 2018")) {
            test.log(LogStatus.PASS, "Dislike video",test.addScreenCapture(capture(webDriver)) + "Berhasil Dislike video");
        } else {
            test.log(LogStatus.FAIL, "Dislike video",test.addScreenCapture(capture(webDriver)) + "Test Gagal");
        }

        Actions builder = new Actions(webDriver);
        Action mouseOverHome = builder
                .moveToElement(webDriver.findElement(By.xpath("//div[contains(text(),'Hi, cobacobabl...')]")))
                .build();

        mouseOverHome.perform();

        bliPlayHomePage.btnLogoutClick();
    }

    // 14th Test
    // Share video
    @Test
    public void ShareVideo () throws InterruptedException, IOException {
        test = report.startTest("Share video");
        BliPlayHomePage bliPlayHomePage = new BliPlayHomePage(webDriver);
        bliPlayHomePage.openBlibliPlay();

        String actual5TC1 = webDriver.findElement(By.xpath("//a[@class='navbar_menu-item browseLink exactActive active']")).getText();
        if (actual5TC1.equals("Beranda")) {
            test.log(LogStatus.PASS,"Buka BlibliPlay", test.addScreenCapture(capture(webDriver)) + "Berhasil membuka BlibliPlay");
        } else {
            test.log(LogStatus.FAIL, "Buka BlibliPlay",test.addScreenCapture(capture(webDriver)) + "Test Gagal");
        }

        bliPlayHomePage.searchVideoIcon();
        bliPlayHomePage.searchVideo(bliPlayProperties.getProperties("properties.key"));
        bliPlayHomePage.enterKey();
        Thread.sleep(2000);

        String actual5TC2 = webDriver.findElement(By.xpath("//div[contains(text(),'Blibli Indonesia Open 2018 Finals')]")).getText();
        if (actual5TC2.contains("Blibli Indonesia Open 2018 Finals")) {
            test.log(LogStatus.PASS, "Cari video Blibli Indonesia Open 2018",test.addScreenCapture(capture(webDriver)) + "Berhasil mencari video");
        } else {
            test.log(LogStatus.FAIL, "Cari video Blibli Indonesia Open 2018",test.addScreenCapture(capture(webDriver)) + "Test Gagal");
        }

        Thread.sleep(2000);
        bliPlayHomePage.btnVideoClick();
        bliPlayHomePage.btnMasukClick();
        Thread.sleep(2000);

        String actual5TC3 = webDriver.findElement(By.xpath("//div[@class='content__description']")).getText();
        if (actual5TC3.equals("Masuk ke akun Blibli Anda untuk menikmati Blibliplay")) {
            test.log(LogStatus.PASS,"Meminta untuk Login",test.addScreenCapture(capture(webDriver)) + "Berhasil meminta untuk login");
        } else {
            test.log(LogStatus.FAIL, "Meminta untuk Login",test.addScreenCapture(capture(webDriver)) + "Test Gagal");
        }

        bliPlayHomePage.btnEmailClick(bliPlayProperties.getProperties("properties.email"));
        bliPlayHomePage.btnPasswordClick(bliPlayProperties.getProperties("properties.password"));
        bliPlayHomePage.btnSubmitLoginClick();
        Thread.sleep(2000);

        String actual5TC4 = webDriver.findElement(By.xpath("//div[contains(text(),'Hi, cobacobabl...')]")).getText();
        if (actual5TC4.equals("Hi, cobacobabl...")) {
            test.log(LogStatus.PASS, "Login",test.addScreenCapture(capture(webDriver)) + "Berhasil login");
        } else {
            test.log(LogStatus.FAIL, "Login",test.addScreenCapture(capture(webDriver)) + "Login Gagal");
        }

        Thread.sleep(2000);
        bliPlayHomePage.btnShareClick();

        String actual5TC5 = webDriver.findElement(By.xpath("//div[@class='modal__contentBox-heading']")).getText();
        if (actual5TC5.contains("Suka Kontennya Ya?")) {
            test.log(LogStatus.PASS, "Share Video",test.addScreenCapture(capture(webDriver)) + "Berhasil share video");
        } else {
            test.log(LogStatus.FAIL, "Share Video",test.addScreenCapture(capture(webDriver)) + "Test Gagal");
        }

        bliPlayHomePage.btnCloseShareClick();


        Actions builder = new Actions(webDriver);
        Action mouseOverHome = builder
                .moveToElement(webDriver.findElement(By.xpath("//div[contains(text(),'Hi, cobacobabl...')]")))
                .build();

        mouseOverHome.perform();

        bliPlayHomePage.btnLogoutClick();
    }

    // 15th Testcase
    // Add to library
    @Test
    public void AddToLibrary () throws InterruptedException, IOException {
        test = report.startTest("Add to library");
        BliPlayHomePage bliPlayHomePage = new BliPlayHomePage(webDriver);
        bliPlayHomePage.openBlibliPlay();

        String actual6TC1 = webDriver.findElement(By.xpath("//a[@class='navbar_menu-item browseLink exactActive active']")).getText();
        if (actual6TC1.equals("Beranda")) {
            test.log(LogStatus.PASS,"Buka BlibliPlay", test.addScreenCapture(capture(webDriver)) + "Berhasil membuka BlibliPlay");
        } else {
            test.log(LogStatus.FAIL, "Buka BlibliPlay",test.addScreenCapture(capture(webDriver)) + "Test Gagal");
        }

        bliPlayHomePage.searchVideoIcon();
        bliPlayHomePage.searchVideo(bliPlayProperties.getProperties("properties.bahadur"));
        bliPlayHomePage.enterKey();
        Thread.sleep(2000);

        String actual6TC2 = webDriver.findElement(By.xpath("//div[@class='card__title'][contains(text(),'BAHADUR')]")).getText();
        if (actual6TC2.contains("BAHADUR")) {
            test.log(LogStatus.PASS, "Cari video BAHADUR",test.addScreenCapture(capture(webDriver)) + "Berhasil mencari video");
        } else {
            test.log(LogStatus.FAIL, "Cari video BAHADUR",test.addScreenCapture(capture(webDriver)) + "Test Gagal");
        }

        Thread.sleep(2000);
        bliPlayHomePage.videoBahadurClick();
        bliPlayHomePage.btnMasukClick();
        Thread.sleep(2000);

        String actual6TC3 = webDriver.findElement(By.xpath("//div[@class='content__description']")).getText();
        if (actual6TC3.equals("Masuk ke akun Blibli Anda untuk menikmati Blibliplay")) {
            test.log(LogStatus.PASS,"Meminta untuk Login",test.addScreenCapture(capture(webDriver)) + "Berhasil meminta untuk login");
        } else {
            test.log(LogStatus.FAIL, "Meminta untuk Login",test.addScreenCapture(capture(webDriver)) + "Test Gagal");
        }

        bliPlayHomePage.btnEmailClick(bliPlayProperties.getProperties("properties.email"));
        bliPlayHomePage.btnPasswordClick(bliPlayProperties.getProperties("properties.password"));
        bliPlayHomePage.btnSubmitLoginClick();
        Thread.sleep(2000);

        String actual6TC4 = webDriver.findElement(By.xpath("//div[contains(text(),'Hi, cobacobabl...')]")).getText();
        if (actual6TC4.equals("Hi, cobacobabl...")) {
            test.log(LogStatus.PASS, "Login",test.addScreenCapture(capture(webDriver)) + "Berhasil login");
        } else {
            test.log(LogStatus.FAIL, "Login",test.addScreenCapture(capture(webDriver)) + "Login Gagal");
        }

        Thread.sleep(2000);
        bliPlayHomePage.addToLibraryClick();

        String actual6TC5 = webDriver.findElement(By.xpath("//div[@class='videoInfo__right-title']")).getText();
        if (actual6TC5.contains("BAHADUR")) {
            test.log(LogStatus.PASS, "Menambahkan ke koleksi",test.addScreenCapture(capture(webDriver)) + "Berhasil Menambahkan video ke koleksi");
        } else {
            test.log(LogStatus.FAIL, "Menambahkan ke koleksi",test.addScreenCapture(capture(webDriver)) + "Test Gagal");
        }

        bliPlayHomePage.openKoleksi();

        String actual6TC7 = webDriver.findElement(By.xpath("//div[@class='library_container-headerSection-leftBlock-heading']")).getText();
        if (actual6TC7.contains("Koleksi")) {
            test.log(LogStatus.PASS, "Membuka koleksi",test.addScreenCapture(capture(webDriver)) + "Berhasil membuka koleksi");
        } else {
            test.log(LogStatus.FAIL, "Membuka koleksi",test.addScreenCapture(capture(webDriver)) + "Test Gagal");
        }

        Actions builder = new Actions(webDriver);
        Action mouseOverHome = builder
                .moveToElement(webDriver.findElement(By.xpath("//div[contains(text(),'Hi, cobacobabl...')]")))
                .build();

        mouseOverHome.perform();

        bliPlayHomePage.btnLogoutClick();
    }

    // 16th Testcase
    // Hapus dari Koleksi
    @Test
    public void RemoveFromLibrary () throws InterruptedException, IOException {
        test = report.startTest("Remove from library");
        BliPlayHomePage bliPlayHomePage = new BliPlayHomePage(webDriver);
        bliPlayHomePage.openBlibliPlay();

        String actual7TC1 = webDriver.findElement(By.xpath("//a[@class='navbar_menu-item browseLink exactActive active']")).getText();
        if (actual7TC1.equals("Beranda")) {
            test.log(LogStatus.PASS,"Buka BlibliPlay", test.addScreenCapture(capture(webDriver)) + "Berhasil membuka BlibliPlay");
        } else {
            test.log(LogStatus.FAIL, "Buka BlibliPlay",test.addScreenCapture(capture(webDriver)) + "Test Gagal");
        }

        bliPlayHomePage.btnMasukClick();
        bliPlayHomePage.btnEmailClick(bliPlayProperties.getProperties("properties.email"));
        bliPlayHomePage.btnPasswordClick(bliPlayProperties.getProperties("properties.password"));
        bliPlayHomePage.btnSubmitLoginClick();
        Thread.sleep(3000);

        String actual7TC2 = webDriver.findElement(By.xpath("//div[contains(text(),'Hi, cobacobabl...')]")).getText();
        if (actual7TC2.equals("Hi, cobacobabl...")) {
            test.log(LogStatus.PASS,"Login ke BlibliPlay", test.addScreenCapture(capture(webDriver)) + "Login Berhasil");
        } else {
            test.log(LogStatus.FAIL, "Login ke BlibliPlay",test.addScreenCapture(capture(webDriver)) + "Login Gagal");
        }

        bliPlayHomePage.openKoleksi();
        String actual7TC3 = webDriver.findElement(By.xpath("//div[@class='library_container-headerSection-leftBlock-heading']")).getText();
        if (actual7TC3.contains("Koleksi")) {
            test.log(LogStatus.PASS, "Membuka koleksi",test.addScreenCapture(capture(webDriver)) + "Berhasil membuka koleksi");
        } else {
            test.log(LogStatus.FAIL, "Membuka koleksi",test.addScreenCapture(capture(webDriver)) + "Test Gagal");
        }

        bliPlayHomePage.openBahadurInLibrary();
        bliPlayHomePage.removeFromLibraryClick();

        String actual7TC4 = webDriver.findElement(By.xpath("//div[@class='videoInfo__right-title']")).getText();
        if (actual7TC4.contains("BAHADUR")) {
            test.log(LogStatus.PASS, "Menghapus dari koleksi",test.addScreenCapture(capture(webDriver)) + "Berhasil menghapus video dari koleksi");
        } else {
            test.log(LogStatus.FAIL, "Menghapus dari koleksi",test.addScreenCapture(capture(webDriver)) + "Test Gagal");
        }

        bliPlayHomePage.openKoleksi();

        String actual7TC5 = webDriver.findElement(By.xpath("//div[@class='library_container-headerSection-leftBlock-heading']")).getText();
        if (actual7TC5.contains("Koleksi")) {
            test.log(LogStatus.PASS, "Membuka koleksi",test.addScreenCapture(capture(webDriver)) + "Berhasil membuka koleksi");
        } else {
            test.log(LogStatus.FAIL, "Membuka koleksi",test.addScreenCapture(capture(webDriver)) + "Test Gagal");
        }

        Actions builder = new Actions(webDriver);
        Action mouseOverHome = builder
                .moveToElement(webDriver.findElement(By.xpath("//div[contains(text(),'Hi, cobacobabl...')]")))
                .build();

        mouseOverHome.perform();

        bliPlayHomePage.btnLogoutClick();
    }

    // 17th Testcase
    // Open Library
    @Test
    public void OpenLibrary () throws InterruptedException, IOException {
        test = report.startTest("Open library");
        BliPlayHomePage bliPlayHomePage = new BliPlayHomePage(webDriver);
        bliPlayHomePage.openBlibliPlay();

        String actual18TC1 = webDriver.findElement(By.xpath("//a[@class='navbar_menu-item browseLink exactActive active']")).getText();
        if (actual18TC1.equals("Beranda")) {
            test.log(LogStatus.PASS,"Buka BlibliPlay", test.addScreenCapture(capture(webDriver)) + "Berhasil membuka BlibliPlay");
        } else {
            test.log(LogStatus.FAIL, "Buka BlibliPlay",test.addScreenCapture(capture(webDriver)) + "Test Gagal");
        }

        bliPlayHomePage.btnMasukClick();
        bliPlayHomePage.btnEmailClick(bliPlayProperties.getProperties("properties.email"));
        bliPlayHomePage.btnPasswordClick(bliPlayProperties.getProperties("properties.password"));
        bliPlayHomePage.btnSubmitLoginClick();
        Thread.sleep(1000);

        String actual2TC4 = webDriver.findElement(By.xpath("//div[contains(text(),'Hi, cobacobabl...')]")).getText();
        if (actual2TC4.equals("Hi, cobacobabl...")) {
            test.log(LogStatus.PASS, "Login", test.addScreenCapture(capture(webDriver)) + "Berhasil login");
        } else {
            test.log(LogStatus.FAIL, "Login", test.addScreenCapture(capture(webDriver)) + "Login Gagal");
        }

        bliPlayHomePage.openKoleksi();
        String actual7TC3 = webDriver.findElement(By.xpath("//div[@class='library_container-headerSection-leftBlock-heading']")).getText();
        if (actual7TC3.contains("Koleksi")) {
            test.log(LogStatus.PASS, "Membuka koleksi",test.addScreenCapture(capture(webDriver)) + "Berhasil membuka koleksi");
        } else {
            test.log(LogStatus.FAIL, "Membuka koleksi",test.addScreenCapture(capture(webDriver)) + "Test Gagal");
        }

        Actions builder = new Actions(webDriver);
        Action mouseOverHome = builder
                .moveToElement(webDriver.findElement(By.xpath("//div[contains(text(),'Hi, cobacobabl...')]")))
                .build();

        mouseOverHome.perform();

        bliPlayHomePage.btnLogoutClick();

    }

    // 18th Testcase
    // Edit First Name
    @Test
    public void ChangeFirstNameOfAccount () throws InterruptedException, IOException {
        test = report.startTest("Change first name of account");
        BliPlayHomePage bliPlayHomePage = new BliPlayHomePage(webDriver);
        bliPlayHomePage.openBlibliPlay();

        String actual13TC1 = webDriver.findElement(By.xpath("//a[@class='navbar_menu-item browseLink exactActive active']")).getText();
        if (actual13TC1.equals("Beranda")) {
            test.log(LogStatus.PASS,"Buka BlibliPlay", test.addScreenCapture(capture(webDriver)) + "Berhasil membuka BlibliPlay");
        } else {
            test.log(LogStatus.FAIL, "Buka BlibliPlay",test.addScreenCapture(capture(webDriver)) + "Test Gagal");
        }

        bliPlayHomePage.btnMasukClick();
        bliPlayHomePage.btnEmailClick(bliPlayProperties.getProperties("properties.email"));
        bliPlayHomePage.btnPasswordClick(bliPlayProperties.getProperties("properties.password"));
        bliPlayHomePage.btnSubmitLoginClick();
        Thread.sleep(3000);

        String actual13TC2 = webDriver.findElement(By.xpath("//div[contains(text(),'Hi, cobacobabl...')]")).getText();
        if (actual13TC2.equals("Hi, cobacobabl...")) {
            test.log(LogStatus.PASS,"Login ke BlibliPlay", test.addScreenCapture(capture(webDriver)) + "Login Berhasil");
        } else {
            test.log(LogStatus.FAIL, "Login ke BlibliPlay",test.addScreenCapture(capture(webDriver)) + "Login Gagal");
        }

        Actions builder = new Actions(webDriver);
        Action mouseOverHome = builder
                .moveToElement(webDriver.findElement(By.xpath("//div[@class='navbar_user-loggedin-username']")))
                .build();
        mouseOverHome.perform();

        bliPlayHomePage.btnAkunClick();
        Thread.sleep(3000);
        Action mouseOverAkun = builder
                .moveToElement(webDriver.findElement(By.xpath("//div[@class='profileDetails__container-header-heading']")))
                .build();
        mouseOverAkun.perform();

        String actual13TC3 = webDriver.findElement(By.xpath("//div[@class='profileDetails__container-header-heading']")).getText();
        if (actual13TC3.equals("Akun")) {
            test.log(LogStatus.PASS,"Membuka Akun", test.addScreenCapture(capture(webDriver)) + "Berhasil membuka Akun");
        } else {
            test.log(LogStatus.FAIL, "Membuka Akun",test.addScreenCapture(capture(webDriver)) + "Test Gagal");
        }

        bliPlayHomePage.btnUbahProfile();
        bliPlayHomePage.btnFirstNameClick();
        bliPlayHomePage.selectAllInFirstName();
        Thread.sleep(2000);
        bliPlayHomePage.btnFillFirstName(bliPlayProperties.getProperties("properties.firstName"));
        Thread.sleep(2000);
        bliPlayHomePage.btnSaveProfile();
        String actual13TC4 = webDriver.findElement(By.xpath("//div[contains(text(),'Ivani Firous')]")).getText();
        if (actual13TC4.equals("Ivani Firous")) {
            test.log(LogStatus.PASS,"Mengedit akun", test.addScreenCapture(capture(webDriver)) + "Berhasil mengedit Akun");
        } else {
            test.log(LogStatus.FAIL, "Mengedit akun",test.addScreenCapture(capture(webDriver)) + "Test Gagal");
        }

        Thread.sleep(3000);
        bliPlayHomePage.btnUbahProfile();
        bliPlayHomePage.btnFirstNameClick();
        bliPlayHomePage.selectAllInFirstName();
        bliPlayHomePage.btnFillFirstName(bliPlayProperties.getProperties("properties.email"));
        bliPlayHomePage.btnSaveProfile();

        mouseOverHome.perform();
        bliPlayHomePage.btnLogoutClick();
    }

    // 19th Testcase
    // Edit Last Name
    @Test
    public void ChangeLastNameOfAccount () throws InterruptedException, IOException {
        test = report.startTest("Change last name of account");
        BliPlayHomePage bliPlayHomePage = new BliPlayHomePage(webDriver);
        bliPlayHomePage.openBlibliPlay();

        String actual13TC1 = webDriver.findElement(By.xpath("//a[@class='navbar_menu-item browseLink exactActive active']")).getText();
        if (actual13TC1.equals("Beranda")) {
            test.log(LogStatus.PASS,"Buka BlibliPlay", test.addScreenCapture(capture(webDriver)) + "Berhasil membuka BlibliPlay");
        } else {
            test.log(LogStatus.FAIL, "Buka BlibliPlay",test.addScreenCapture(capture(webDriver)) + "Test Gagal");
        }

        bliPlayHomePage.btnMasukClick();
        bliPlayHomePage.btnEmailClick(bliPlayProperties.getProperties("properties.email"));
        bliPlayHomePage.btnPasswordClick(bliPlayProperties.getProperties("properties.password"));
        bliPlayHomePage.btnSubmitLoginClick();
        Thread.sleep(3000);

        String actual13TC2 = webDriver.findElement(By.xpath("//div[contains(text(),'Hi, cobacobabl...')]")).getText();
        if (actual13TC2.equals("Hi, cobacobabl...")) {
            test.log(LogStatus.PASS,"Login ke BlibliPlay", test.addScreenCapture(capture(webDriver)) + "Login Berhasil");
        } else {
            test.log(LogStatus.FAIL, "Login ke BlibliPlay",test.addScreenCapture(capture(webDriver)) + "Login Gagal");
        }

        Actions builder = new Actions(webDriver);
        Action mouseOverHome = builder
                .moveToElement(webDriver.findElement(By.xpath("//div[@class='navbar_user-loggedin-username']")))
                .build();
        mouseOverHome.perform();

        bliPlayHomePage.btnAkunClick();
        Thread.sleep(3000);
        Action mouseOverAkun = builder
                .moveToElement(webDriver.findElement(By.xpath("//div[@class='profileDetails__container-header-heading']")))
                .build();
        mouseOverAkun.perform();

        String actual13TC3 = webDriver.findElement(By.xpath("//div[@class='profileDetails__container-header-heading']")).getText();
        if (actual13TC3.equals("Akun")) {
            test.log(LogStatus.PASS,"Membuka Akun", test.addScreenCapture(capture(webDriver)) + "Berhasil membuka Akun");
        } else {
            test.log(LogStatus.FAIL, "Membuka Akun",test.addScreenCapture(capture(webDriver)) + "Test Gagal");
        }

        bliPlayHomePage.btnUbahProfile();
        Thread.sleep(2000);
        bliPlayHomePage.btnLastNameClick();
        bliPlayHomePage.selectAllInLastName();
        Thread.sleep(2000);
        bliPlayHomePage.btnFillLastName(bliPlayProperties.getProperties("properties.marga"));
        Thread.sleep(2000);
        bliPlayHomePage.btnSaveProfile();
        String actual13TC4 = webDriver.findElement(By.xpath("//div[contains(text(),'Cobacobablibli@gmail.com Purba')]")).getText();
        if (actual13TC4.equals("Cobacobablibli@gmail.com Purba")) {
            test.log(LogStatus.PASS,"Mengedit akun", test.addScreenCapture(capture(webDriver)) + "Berhasil mengedit Akun");
        } else {
            test.log(LogStatus.FAIL, "Mengedit akun",test.addScreenCapture(capture(webDriver)) + "Test Gagal");
        }

        Thread.sleep(3000);
        bliPlayHomePage.btnUbahProfile();
        bliPlayHomePage.btnLastNameClick();
        bliPlayHomePage.selectAllInLastName();
        bliPlayHomePage.btnFillLastName(bliPlayProperties.getProperties("properties.lastName"));
        bliPlayHomePage.btnSaveProfile();

        mouseOverHome.perform();
        bliPlayHomePage.btnLogoutClick();
    }

    // 20th Test
    // Tentang BlibliPlay (footer BlibliPlay)
    @Test
    public void OpenTentangBlibliplay() throws InterruptedException, IOException {
        test = report.startTest("Open Tentang Blibliplay");
        BliPlayHomePage bliPlayHomePage = new BliPlayHomePage(webDriver);

        bliPlayHomePage.openBlibliPlay();
        Thread.sleep(2000);
        String actual12TC1 = webDriver.findElement(By.xpath("//a[@class='navbar_menu-item browseLink exactActive active']")).getText();
        if (actual12TC1.equals("Beranda")) {
            test.log(LogStatus.PASS,"Buka BlibliPlay", test.addScreenCapture(capture(webDriver)) + "Berhasil membuka BlibliPlay");
        } else {
            test.log(LogStatus.FAIL,"Buka BlibliPlay", test.addScreenCapture(capture(webDriver)) + "Test Gagal");
        }

        bliPlayHomePage.searchVideoIcon();
        bliPlayHomePage.searchVideo(bliPlayProperties.getProperties("properties.key"));
        bliPlayHomePage.enterKey();

        Thread.sleep(3000);
        bliPlayHomePage.tentangBliPlayClick();
        Thread.sleep(3000);

        String actual12TC2 = webDriver.findElement(By.xpath("//div[@class='about__head']")).getText();
        if(actual12TC2.equals("Tentang Blibliplay")) {
            test.log(LogStatus.PASS,"Membuka Tentang BlibliPlay", test.addScreenCapture(capture(webDriver)) +"Berhasil membuka Tentang BlibliPlay");
        } else
        {
            test.log(LogStatus.FAIL,"Membuka Tentang BlibliPlay",test.addScreenCapture(capture(webDriver)) + "Test gagal");
        }
    }


//    // 14th Testcase
//    // Kemeriahan Blibli 13th Anniversary (1 Highlight)
//    @Test
//    public void Blibli14thTest() throws InterruptedException, IOException {
//
//        BliPlayHomePage bliPlayHomePage = new BliPlayHomePage(webDriver);
//        test = report.startTest("BlibliPlayTest14");
//
//        bliPlayHomePage.openBlibliPlay();
//
//        String actual11TC1 = webDriver.findElement(By.xpath("//a[@class='navbar_menu-item browseLink exactActive active']")).getText();
//        if (actual11TC1.equals("Beranda")) {
//            test.log(LogStatus.PASS,"Buka BlibliPlay", test.addScreenCapture(capture(webDriver)) + "Berhasil membuka BlibliPlay");
//        } else {
//            test.log(LogStatus.FAIL,"Buka BlibliPlay", test.addScreenCapture(capture(webDriver)) + "Test Gagal");
//        }
//
//        bliPlayHomePage.btnMasukClick();
//        bliPlayHomePage.btnEmailClick(bliPlayProperties.getProperties("properties.email"));
//        bliPlayHomePage.btnPasswordClick(bliPlayProperties.getProperties("properties.password"));
//        bliPlayHomePage.btnSubmitLoginClick();
//        Thread.sleep(3000);
//
//        String actual11TC2 = webDriver.findElement(By.xpath("//div[contains(text(),'Hi, cobacobabl...')]")).getText();
//        if (actual11TC2.equals("Hi, cobacobabl...")) {
//            test.log(LogStatus.PASS,"Login ke BlibliPlay", test.addScreenCapture(capture(webDriver)) + "Login Berhasil");
//        } else {
//            test.log(LogStatus.FAIL, "Login ke BlibliPlay",test.addScreenCapture(capture(webDriver)) + "Login Gagal");
//        }
//
//        bliPlayHomePage.searchVideoIcon();
//        bliPlayHomePage.searchVideo(bliPlayProperties.getProperties("properties.goresanJejak"));
//        bliPlayHomePage.enterKey();
//
//        String actual8TC1 = webDriver.findElement(By.xpath("//div[@class='card__title']")).getText();
//        if (actual8TC1.equals("GORESAN JEJAK")) {
//            test.log(LogStatus.PASS, "Mencari video Goresan Jejak",test.addScreenCapture(capture(webDriver)) + "Berhasil mencari video");
//        } else {
//            test.log(LogStatus.FAIL, "Mencari video Goresan Jejak",test.addScreenCapture(capture(webDriver)) + "Test Gagal");
//        }
//
//        bliPlayHomePage.tontonGoresanJejak();
//        String actual8TC3 = webDriver.findElement(By.xpath("//div[@class='videoInfo-title']")).getText();
//        if (actual8TC3.equals("Goresan Jejak #1 - PROLOG | 01")) {
//            test.log(LogStatus.PASS, "Menonton video Goresan Jejak",test.addScreenCapture(capture(webDriver)) + "Berhasil menonton video");
//        } else {
//            test.log(LogStatus.FAIL,"Menonton video Goresan Jejak", test.addScreenCapture(capture(webDriver)) + "Test Gagal");
//        }
//        Thread.sleep(5000);
//
//        Actions builder = new Actions(webDriver);
//        Action mouseOverPause = builder
//                .moveToElement(webDriver.findElement(By.xpath(" //section[@class='container']")))
//                .build();
//        mouseOverPause.perform();
//        bliPlayHomePage.pauseClick();
//        Thread.sleep(5000);
//
//        Action mouseOverHome = builder
//                .moveToElement(webDriver.findElement(By.xpath("//div[contains(text(),'Hi, cobacobabl...')]")))
//                .build();
//        mouseOverHome.perform();
//    }


    //    // 13th Testcase
//    // The Big Start Indonesia
//   @Test
//    public void Blibli13thTest() throws InterruptedException, IOException {
//        test = report.startTest("BlibliPlayTest13");
//        BliPlayHomePage bliPlayHomePage = new BliPlayHomePage(webDriver);
//        bliPlayHomePage.openBlibliPlay();
//
//       String actual13TC1 = webDriver.findElement(By.xpath("//a[@class='navbar_menu-item browseLink exactActive active']")).getText();
//       if (actual13TC1.equals("Beranda")) {
//           test.log(LogStatus.PASS,"Buka BlibliPlay", test.addScreenCapture(capture(webDriver)) + "Berhasil membuka BlibliPlay");
//       } else {
//           test.log(LogStatus.FAIL,"Buka BlibliPlay", test.addScreenCapture(capture(webDriver)) + "Test Gagal");
//       }
//
//       bliPlayHomePage.btnMasukClick();
//       bliPlayHomePage.btnEmailClick(bliPlayProperties.getProperties("properties.email"));
//       bliPlayHomePage.btnPasswordClick(bliPlayProperties.getProperties("properties.password"));
//       bliPlayHomePage.btnSubmitLoginClick();
//       Thread.sleep(3000);
//
//       String actual13TC2 = webDriver.findElement(By.xpath("//div[contains(text(),'Hi, cobacobabl...')]")).getText();
//       if (actual13TC2.equals("Hi, cobacobabl...")) {
//           test.log(LogStatus.PASS,"Login ke BlibliPlay", test.addScreenCapture(capture(webDriver)) + "Login Berhasil");
//       } else {
//           test.log(LogStatus.FAIL, "Login ke BlibliPlay",test.addScreenCapture(capture(webDriver)) + "Login Gagal");
//       }
//
//       bliPlayHomePage.searchVideoIcon();
//        bliPlayHomePage.searchVideo(bliPlayProperties.getProperties("properties.theBigStart"));
//        bliPlayHomePage.enterKey();
//
//        String actual13TC3 = webDriver.findElement(By.xpath("//div[@class='search__container-heading']")).getText();
//        if(actual13TC3.equals("Hasil pencarian untuk \"The Big Start Indonesia\"")) {
//            test.log(LogStatus.PASS, "Mencari video The Big Start Indonesia",test.addScreenCapture(capture(webDriver)) +"Berhasil mencari video");
//        } else
//        {
//            test.log(LogStatus.FAIL,"Mencari video The Big Start Indonesia",test.addScreenCapture(capture(webDriver)) + "Test Gagal");
//        }
//
//        bliPlayHomePage.theBigStartClick();
//        Thread.sleep(3000);
//
//       bliPlayHomePage.seasonTheBigStart();
//       //bliPlayHomePage.secondSeasonTheBigStart();
//
//        Actions builder = new Actions(webDriver);
//        Action mouseOverHome = builder
//                .moveToElement(webDriver.findElement(By.xpath("//div[contains(text(),'Hi, cobacobabl...')]")))
//                .build();
//        mouseOverHome.perform();
//
//        bliPlayHomePage.btnLogoutClick();
//    }
//


//    // 13th Test
//    // Belanja di Blibli.com (footer BlibliPlay)
//    @Test
//    public void Blibli13thTest() throws InterruptedException, IOException {
//        test = report.startTest("BlibliPlayTest13");
//        BliPlayHomePage bliPlayHomePage = new BliPlayHomePage(webDriver);
//        BlibliHomePage blibliHomePage = new BlibliHomePage(webDriver);
//
//        bliPlayHomePage.openBlibliPlay();
//        Thread.sleep(2000);
//        String actual13TC1 = webDriver.findElement(By.xpath("//a[@class='navbar_menu-item browseLink exactActive active']")).getText();
//        if (actual13TC1.equals("Beranda")) {
//            test.log(LogStatus.PASS,"Buka BlibliPlay", test.addScreenCapture(capture(webDriver)) + "Berhasil membuka BlibliPlay");
//        } else {
//            test.log(LogStatus.FAIL,"Buka BlibliPlay", test.addScreenCapture(capture(webDriver)) + "Test Gagal");
//        }
//
//        bliPlayHomePage.belanjaDiBlibliClick();
//        Thread.sleep(2000);
//
//        String MainWindow=webDriver.getWindowHandle();
//        webDriver.switchTo().window(MainWindow);
//        webDriver.close();
//        Thread.sleep(3000);
//
//        String actual4TC3 = webDriver.findElement(By.xpath("//a[@class='link'][contains(text(),'Jual di Blibli')]")).getText();
//        if(actual4TC3.equals("Jual di Blibli")) {
//            test.log(LogStatus.PASS, test.addScreenCapture(capture(webDriver)) +"Navigated to the specified URL");
//        } else
//        {
//            test.log(LogStatus.FAIL,test.addScreenCapture(capture(webDriver)) + "Test Failed");
//        }
//    }

}
